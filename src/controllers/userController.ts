import { Request, Response } from 'express'
import  { Service } from 'typedi'
import {DataService} from '../services/userService'
import Controller from './controller'

@Service()
class UserController implements Controller {
  constructor ( private readonly userService: DataService ) { }

  public async get (_req: Request, res: Response):Promise<any> {
    const result: string[] = await this.userService.getAll();
    res.set('Content-Type', 'text/html');
    return res.send(
      Buffer.from(
        result.map(user => `<h2 style="color:#488375; text-align: center">${user}</h2>`)
        .reduce((a, b) => a + b)
    ));
  }
}

export default UserController
