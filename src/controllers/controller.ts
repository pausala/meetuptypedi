import { Request, Response } from 'express'
export default interface Controller {
    get(req: Request, res: Response): Promise<any>;
}
