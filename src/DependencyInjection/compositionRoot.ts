import Controller from '../controllers/controller';
import UserController from '../controllers/userController';
import Container from 'typedi';
import { UserRepositoryDev, UserRepositoryProd } from '../respositories/UserRepository';

export abstract class CompositionRoot {

  public static setEnvirontment(){

    if (process.env.EXECUTION_MODE === 'prod') {
      console.log('\x1b[31m%s\x1b[0m', `🔥 ${process.env.EXECUTION_MODE.toUpperCase()} mode 🔥`);
      this.prodEnv();
    } 
    else if(process.env.EXECUTION_MODE === 'dev') {
      console.log('\x1b[36m%s\x1b[0m', `🌿 ${process.env.EXECUTION_MODE.toUpperCase()} mode 🌿`);
      this.devEnv();
    }
    else{
      throw new Error('process.env.EXECUTION_MODE is not valid');
    }

  }

  private static devEnv = () => {
    Container.set([{ id: 'userRepo', value: new UserRepositoryDev() }]);
  }

  private static  prodEnv = () => {
    Container.set([{ id: 'userRepo', value: new UserRepositoryProd() }])
  }

  public static getController = (key:string):Controller => {

    switch(key){
      case 'UserController': return Container.get(UserController);
      default: throw new Error(`No controller matches given key:  ${key}`);
    }

  }

}
