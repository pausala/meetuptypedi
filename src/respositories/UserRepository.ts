import { Service } from 'typedi'
import User from '../models/User'
import { UserRepository } from './Repository'

@Service()
export class UserRepositoryDev implements UserRepository {
  private name: string;
  private readonly users: User[] = [
    { name: '🌿 Development Mode User 🌿', age:20 }
  ];

  getAll = () => Promise.resolve(this.users);
}

@Service()
export class UserRepositoryProd implements UserRepository {
  private readonly users: User[] = [
    { name: '🔥 Production Mode User 🔥', age:20  }
  ];

  getAll = () => Promise.resolve(this.users);
}
