import User from '../models/User'

export interface UserRepository{
   getAll:() => Promise<User[]>;
}
