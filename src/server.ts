'use strict'
import 'reflect-metadata'

import express from 'express'
import { CompositionRoot } from './DependencyInjection/compositionRoot'
import Controller from './controllers/controller'

// Constants
const PORT = 8080
const HOST = '0.0.0.0'

// App
const main = async () => {

  const app = express()

  /**init Composition root */
  CompositionRoot.setEnvirontment();

  /** Set routes */
  const userController : Controller = CompositionRoot.getController('UserController');
  app.get('/', (req, res) => userController.get(req, res))


  /** Start app */
  app.listen(PORT, HOST, () => {
    console.log(`Running on ->  http://${HOST}:${PORT}`)
  })
}

main().catch((err) => {
  console.log(err)
})
