import Container, { Inject, Service } from 'typedi'
import User from '../models/User'
import { UserRepository } from '../respositories/Repository'


interface Factory {  create(): void;}

@Service()
export class UserServiceFactory implements Factory{
  create(){
    if (process.env.EXECUTION_MODE === 'prod') {
      return new UserService(Container.get('userRepo'))
    }else{
      return new UserServiceDev(Container.get('userRepo'))
    }
  }
}

@Service({ factory: [UserServiceFactory, 'create'] })
export abstract class DataService{
  abstract getAll():Promise<string[]>;
}

export class UserServiceDev extends DataService{
  constructor (@Inject('userRepo') private readonly userRepository: UserRepository) { 
    super();
  }
  public getAll = async ():Promise<string[]> => {
    const result:Promise<User[]> = this.userRepository.getAll();
    const users = await result;
    return users.map(user => `${user.name} → Age : ${user.age}`);
  }
}

export class UserService implements DataService {
  constructor (@Inject('userRepo') private readonly userRepository: UserRepository) { }
  public getAll = async ():Promise<string[]> => {
    const result:Promise<User[]> = this.userRepository.getAll();
    const users = await result;
    return users.map(user =>  user.name);
  }
}



