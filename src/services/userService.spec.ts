import UserService from './userService'
import { UserRepositoryDev } from '../respositories/UserRepository'

describe('userService', () => {
  const service : UserService = new UserService(new UserRepositoryDev())

  it('Should return user array', async () => {
    const users = await service.getAll()
    expect(users.length).toBeGreaterThan(0)
  })
})
