'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const express_1 = __importDefault(require("express"));
const compositionRoot_1 = require("./DependencyInjection/compositionRoot");
// Constants
const PORT = 8080;
const HOST = '0.0.0.0';
// App
const main = () => __awaiter(void 0, void 0, void 0, function* () {
    const app = (0, express_1.default)();
    /**init Composition root */
    compositionRoot_1.CompositionRoot.setEnvirontment();
    /** Set routes */
    const userController = compositionRoot_1.CompositionRoot.getController('UserController');
    app.get('/', (req, res) => userController.get(req, res));
    /** Start app */
    app.listen(PORT, HOST, () => {
        console.log(`Running on ->  http://${HOST}:${PORT}`);
    });
});
main().catch((err) => {
    console.log(err);
});
