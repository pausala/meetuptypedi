"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepositoryProd = exports.UserRepositoryDev = void 0;
const typedi_1 = require("typedi");
let UserRepositoryDev = class UserRepositoryDev {
    constructor() {
        this.users = [
            { name: '🌿 Development Mode User 🌿', age: 20 }
        ];
        this.getAll = () => Promise.resolve(this.users);
    }
};
UserRepositoryDev = __decorate([
    (0, typedi_1.Service)()
], UserRepositoryDev);
exports.UserRepositoryDev = UserRepositoryDev;
let UserRepositoryProd = class UserRepositoryProd {
    constructor() {
        this.users = [
            { name: '🔥 Production Mode User 🔥', age: 20 }
        ];
        this.getAll = () => Promise.resolve(this.users);
    }
};
UserRepositoryProd = __decorate([
    (0, typedi_1.Service)()
], UserRepositoryProd);
exports.UserRepositoryProd = UserRepositoryProd;
