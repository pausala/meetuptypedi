"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setEnvirontment = void 0;
const typedi_1 = __importDefault(require("typedi"));
const UserRepository_1 = require("../respositories/UserRepository");
const setEnvirontment = () => {
    if (process.env.EXECUTION_MODE === 'prod') {
        console.log('\x1b[31m%s\x1b[0m', `🔥 ${process.env.EXECUTION_MODE.toUpperCase()} mode 🔥`);
        prodEnv();
    }
    else {
        console.log('\x1b[36m%s\x1b[0m', `🌿 ${process.env.EXECUTION_MODE.toUpperCase()} mode 🌿`);
        devEnv();
    }
};
exports.setEnvirontment = setEnvirontment;
const devEnv = () => {
    typedi_1.default.set([{ id: 'userRepo', value: new UserRepository_1.UserRepositoryDev() }]);
};
const prodEnv = () => {
    typedi_1.default.set([{ id: 'userRepo', value: new UserRepository_1.UserRepositoryProd() }]);
};
