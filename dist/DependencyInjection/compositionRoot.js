"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompositionRoot = void 0;
const userController_1 = __importDefault(require("../controllers/userController"));
const typedi_1 = __importDefault(require("typedi"));
const UserRepository_1 = require("../respositories/UserRepository");
class CompositionRoot {
    static setEnvirontment() {
        if (process.env.EXECUTION_MODE === 'prod') {
            console.log('\x1b[31m%s\x1b[0m', `🔥 ${process.env.EXECUTION_MODE.toUpperCase()} mode 🔥`);
            this.prodEnv();
        }
        else if (process.env.EXECUTION_MODE === 'dev') {
            console.log('\x1b[36m%s\x1b[0m', `🌿 ${process.env.EXECUTION_MODE.toUpperCase()} mode 🌿`);
            this.devEnv();
        }
        else {
            throw new Error('process.env.EXECUTION_MODE is not valid');
        }
    }
}
exports.CompositionRoot = CompositionRoot;
CompositionRoot.devEnv = () => {
    typedi_1.default.set([{ id: 'userRepo', value: new UserRepository_1.UserRepositoryDev() }]);
};
CompositionRoot.prodEnv = () => {
    typedi_1.default.set([{ id: 'userRepo', value: new UserRepository_1.UserRepositoryProd() }]);
};
CompositionRoot.getController = (key) => {
    switch (key) {
        case 'UserController': return typedi_1.default.get(userController_1.default);
        default: throw new Error(`No controller matches given key:  ${key}`);
    }
};
